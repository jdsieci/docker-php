#!/bin/bash

set -euo pipefail
set -x

image_name=docker.io/jdsieci/php-fpm
srcdir=src

php_version=$1

latest_tag=$(skopeo inspect docker://php:latest | jq -r ".RepoTags[] | select(. | test(\"^${php_version}(.[0-9]+)?-fpm-alpine\"))" | sort -V | tail -1)

version=$(echo -n ${latest_tag} | cut -d '-' -f1)
ver_major=$(echo -n ${version} | cut -d '.' -f1)
ver_minor=$(echo -n ${version} | cut -d '.' -f2)
ver_patch=$(echo -n ${version} | cut -d '.' -f3)

typeset -i ver_short="${ver_major}${ver_minor}"

buildroot=build/${ver_short}

template=Dockerfile.template

if [[ ${ver_major} -lt 7 ]];then
  template=Dockerfile_5.template
elif [[ ${ver_short} -lt 72 ]];then
  template=Dockerfile_71.template
elif [[ ${ver_short} -lt 73 ]]; then
  template=Dockerfile_72.template
elif [[ ${ver_short} -lt 74 ]]; then
  template=Dockerfile_73.template
elif [[ ${ver_short} -lt 80 ]]; then
  template=Dockerfile_74.template
fi

mkdir -p ${buildroot}
cp -Ppr ${srcdir}/* ${buildroot}

#if [[ ${ver_short} -lt 80 ]] && [[ ${ver_short} -gt 71 ]];then
#  wget -O ${buildroot}/enchant.patch "https://aur.archlinux.org/cgit/aur.git/plain/enchant-2.patch?h=php${ver_short}"
#fi

sed 's|%%PHP_VERSION%%|'${latest_tag}'|' ${srcdir}/${template} > ${buildroot}/Dockerfile

podman build -t ${image_name}:${version} \
  -t ${image_name}:${ver_major}.${ver_minor} \
  ${buildroot}


