#!/bin/bash

while read; do
  ./build.sh $REPLY
done < <(skopeo inspect docker://php | jq -r '.RepoTags[] | select( . | test("^[0-9]+.[0-9]+$"))' | sort -V | tail -n +3)

echo "Build completed, pushing images"
./push.sh
