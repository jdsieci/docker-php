#!/bin/bash


while read; do
	echo "Pushing '${REPLY}'"
	podman push "${REPLY}"
done < <(podman image ls docker.io/jdsieci/php-fpm --format json | jq -r '.[].Names[]' | sort -r | uniq)
