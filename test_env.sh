#!/bin/bash

set -euo pipefail

PHP_TAG=${1}

podman run --rm \
  --name=phpfpm \
  --detach \
  -p 9000:9000 \
  --volume ./htdocs:/var/www/html \
  jdsieci/php-fpm:${PHP_TAG}
podman run --rm \
  --publish 8080:80 \
  --name=httpd \
  --detach \
  --volume ./htdocs:/usr/local/apache2/htdocs \
  --volume ./test_httpd.conf:/usr/local/apache2/conf.d/local.conf:ro \
  jdsieci/httpd:2.4

