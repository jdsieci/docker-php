<?php
$pattern = '/^(.*?\.php)/i';
$matches = array();
if(preg_match($pattern, $_SERVER['SCRIPT_NAME'], $matches)){
  $_SERVER['SCRIPT_NAME'] = $matches[1];
  $_SERVER['PHP_SELF'] = $_SERVER['SCRIPT_NAME'].(isset($_SERVER['PATH_INFO'])?$_SERVER['PATH_INFO']:'');
}
// 
if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'){
  $_SERVER['SCRIPT_URI'] = str_replace($_SERVER['REQUEST_SCHEME'], $_SERVER['HTTP_X_FORWARDED_PROTO'], $_SERVER['SCRIPT_URI']);
  $_SERVER['REQUEST_SCHEME'] = $_SERVER['HTTP_X_FORWARDED_PROTO'];
  $_SERVER['HTTPS'] = 'on';
}
if(isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
  $_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
}
if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != $_SERVER['REMOTE_ADDR']) {
  $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
if(isset($_SERVER['HTTP_X_REAL_IP']) && $_SERVER['HTTP_X_REAL_IP'] != $_SERVER['REMOTE_ADDR']) {
  $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REAL_IP'];
}

if(isset($_ENV['HTTP_X_FORWARDED_PROTO']) && $_ENV['HTTP_X_FORWARDED_PROTO'] == 'https'){
  $_ENV['SCRIPT_URI'] = str_replace($_ENV['REQUEST_SCHEME'], $_ENV['HTTP_X_FORWARDED_PROTO'], $_ENV['SCRIPT_URI']);
  $_ENV['REQUEST_SCHEME'] = $_ENV['HTTP_X_FORWARDED_PROTO'];
  $_ENV['HTTPS'] = 'on';
}
if(isset($_ENV['HTTP_X_FORWARDED_FOR']) && $_ENV['HTTP_X_FORWARDED_FOR'] != $_ENV['REMOTE_ADDR']) {
  $_ENV['REMOTE_ADDR'] = $_ENV['HTTP_X_FORWARDED_FOR'];
}
if(isset($_ENV['HTTP_X_REAL_IP']) && $_ENV['HTTP_X_REAL_IP'] != $_ENV['REMOTE_ADDR']) {
  $_ENV['REMOTE_ADDR'] = $_ENV['HTTP_X_REAL_IP'];
}

// Cleanup
unset($matches);
unset($pattern);
?>
