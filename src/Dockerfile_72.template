FROM php:%%PHP_VERSION%%

RUN apk upgrade --no-cache

RUN apk add --no-cache --virtual .persistent-deps \
		bzip2

RUN apk add --no-cache --virtual .build-deps \
		freetype-dev \
		libmcrypt-dev \
		libpng-dev \
		libjpeg-turbo-dev \
		libxpm-dev \
                libwebp-dev \
		libldap \
		libbz2 \
                libzip-dev \
		gmp-dev \
		bzip2-dev \
		libxml2-dev \
		gettext-dev \
		postgresql-dev \
		recode-dev \
		enchant2-dev \
		icu-dev \
                patch

COPY php72/* /usr/src/

RUN docker-php-source extract \
        && touch /usr/src/php/.docker-delete-me \
        && cd /usr/src/php \
        && patch -p1 -i ../php-enchant-php5.3.patch \
        && patch -p1 -i ../php-enchant-depr.patch \
        && cd ${OLDPWD} \
        && docker-php-ext-install iconv \
		exif bz2 gmp bcmath soap gettext \
		pgsql pdo_pgsql recode dba calendar \
		intl enchant zip \
	&& docker-php-ext-configure gd \
                --with-freetype-dir=/usr/include/ \
		--with-png-dir=/usr/include/ \
                --with-jpeg-dir=/usr/include/ \
		--with-xpm-dir=/usr/include \
                --with-webp-dir=/usr/include \
	&& docker-php-ext-install gd \
	&& docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-configure mysqli --with-mysqli=shared,mysqlnd \
	&& docker-php-ext-install mysqli \
	&& runDeps="$( \
		scanelf --needed --nobanner --recursive /usr/local \
			| awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
			| sort -u \
			| xargs -r apk info --installed \
			| sort -u \
	)" \
        && apk add --no-cache --virtual .php-rundeps $runDeps
RUN apk del .build-deps


COPY docker-php-entrypoint /usr/local/bin/

COPY path-fixes.php /usr/local/lib/php/

COPY zz-docker-extra.conf /usr/local/etc/php-fpm.d/

RUN mv -f /usr/local/etc/php-fpm.d /usr/local/etc/php-fpm.image \
        && mkdir -p /usr/local/etc/php-fpm.d \
        && mkdir -p /usr/local/etc/php-fpm.runtime \
        && sed -i '/^include=.*/i include=etc/php-fpm.runtime/*.conf' /usr/local/etc/php-fpm.conf \
        && sed -i '/^include=.*runtime.*/i include=etc/php-fpm.image/*.conf' /usr/local/etc/php-fpm.conf

# vim: ft=dockerfile
